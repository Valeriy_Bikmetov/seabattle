# -*- coding utf-8 -*-


class Manager:
     """
     Класс управления процессом игры
     Содержит атрибуты:
        list_rounds - список ссылок на игровые раунды
        list_users - список активных пользователей
     """
     def __init__(self) -> None:
         lst_rounds: list = list()
         lst_users: list = list()
