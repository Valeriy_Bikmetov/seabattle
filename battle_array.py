# -*- coding utf-8 -*-

from sea_battle import bot
from player import Player
import constants as const
from constants import ValueCells
from constants import ResultShot
from utility import check_boat_coordinate
from utility import check_cell_name
from utility import coordinate_from_str


class BattleArray:
    """
    Класс игрового поля
    Содержит атрибуты:
        bot - ссылка на объект бота
        player - ссылка на объект игрока (владельца поля)
        enemy_field - ссылка на игровое поле противника
        enemy_info - ссылка на информационное игровое поле противника
        field - Список 11 на 11. Это игровое поле. 11 на 11 это для удобства взаимодействия с пользователем.
            Нулевая строка этого списка содержит заголовок колонок #абвгдежзик, 
            а нулевая колонка номера строк от 1 до 10. 
            Пользователь вводит координаты, как например, «д1», а не «д0» и 
            при такой структуре массива нам не надо помнить о том, что нумерация массивов в Python начинается с нуля
    Содержит методы:
        def __init__(self, player, enemy): конструктор. 
            В конструкторе класса надо создать массив field и заполнить его начальными значениями.
        def allocation(self, coordinate): Размещение корабля на поле. 
            Получает в качестве параметра строку координат корабля, 
            начальная и конечная через дефис, на пример, «д4-д7» (вертикально) или «д4-з4» (горизонтально). 
            Должн проверить не идут ли эти координаты зигзагом, определить длину корабля, 
            сколько уже таких кораблей, возможность размещения по этим координатам 
            (не попадают ли они на координаты существующих кораблей, есть ли зазор между этими координатами и 
            существующими кораблями, не выходят ли эти координаты за пределы поля). 
            Если ошибка запустить выдачу сообщения об ошибке, 
            иначе разместить в данных элементах массива соответствующие символы.
        def shot(self, coordinate): Метод анализа вражеского выстрела. 
            Получает в качестве параметра координаты выстрела. 
            Проверяет какой символ находится в массиве по этим координатам. 
            Если это попадание – изменяем символ в это позиции на символ попадания, 
            если нет, то на символ непопадания. 
            Аналогичные действия проводим с информационным полем противника.
        def show(self): Метод формирования текстового отображения игрового поля.
            Построчно считываем массив игрового поля и формируем текст.
    """
    def __init__(self, player: Player) -> None:
        self.bot = bot
        self.player = player
        self.enemy = None
        self.field = list()
        self.field.append(const.title)
        for i in range(1, 11):
            t = [str(i)]
            t += [ValueCells.empty.value for _ in range(10)]
            self.field.append(t)

    async def set_enemy(self, enemy: Player) -> None:
        self.enemy = enemy

    async def get_enemy(self) -> Player:
        return self.enemy

    async def allocation(self, coordinate: str) -> None:
        """ Методом check_boat_coordinate проверяем корректность ввода координат
            Результатом проверки будет список. Если он пуст неправильно заданы координаты
        """
        lst: list = check_boat_coordinate(coordinate)
        if not lst:  # Список пуст, значит неверно заданы координаты
            ...

    async def clear_field(self) -> None:
        """ Очищаем игровое поле.
        Устанавливаем во всех клетках пустое значение"""
        for i in range(1, 11):
            for j in range(1,11):
                self.field[i][j] = ValueCells.empty.value

    async def clear_cell(self, cell: tuple) -> None:
        """ Очищаем конкретную клетку игрового поля """
        i, j = cell
        self.field[i][j] = ValueCells.empty.value

    async def show_field(self) -> None:
        str_field: str = ''
        for i in range(10):
            row = self.field[i]
            s = f'|{"|".join(row)}|'
            str_field += s + "\n"
        text = f'<pre>{str_field}</pre>'
        chat_id = self.player.chat_id
        await self.bot.send_message(chat_id, text, parse_mode='HTML')

    async def shot(self, cell: str) -> ResultShot:
        """ """
        if not check_cell_name(cell):
            chat_id = self.enemy.chat_id
            await self.bot.send_message(chat_id, const.mes_error_cordinat, parse_mode='HTML')
        else:
            row, column = coordinate_from_str(cell)
            ch = self.get_value_cell(row, column)  #
            # Попадание. Заменяем Ж на П в своем поле, проводим изменения в информационном поле противника
            if ch == ValueCells.deck.value:
                await  self.set_value_cell(row, column, ValueCells.hit.value)
                if not self.enemy.battle_array.mylayout:
                    await self.enemy.battle_array.set_value_cell(row, column, ValueCells.hit.value)
                ...
            elif ch == ValueCells.hit.value:  # Попал в уже пораженное место
                ...
            elif ch == ValueCells.empty.value:  # Промахнулся
                ...

    async def get_value_cell(self, row: int, column: int) -> str:
        return self.field[row][column]

    async def set_value_cell(self, row: int, column: int, value: str) -> None:
        self.field[row][column] = value