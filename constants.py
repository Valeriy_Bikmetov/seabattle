# -*- coding utf-8 -*-

from enum import Enum, auto


class ValueCells(Enum):
    deck = 'Ж'  # Символ корабля
    empty = ' '  # Символ пустого места
    hit = 'П'  # Попадание
    shot = '*'  # Выстрел
    col = '|'  # Символ разделителя колонки


class CountBoats(Enum):
    deck_4 = auto()  # Число четырехпалубных кораблей 1
    deck_3 = auto()  # Число трехпалубных кораблей 2
    deck_2 = auto()  # Число двухпалубных кораблей 3
    deck_1 = auto()  # Число катеров 4


class ResultShot(Enum):
    to_miss = 0  # Результат выстрела - промах
    hit = 1 #    # Результат выстрела - попадание


title = list('#абвгдежзик')  # Строка заголовка поля
mes_error_cordinat = 'Неверное задание координаты'
mes_hit = 'Попадание'
mes_miss = 'Промах'