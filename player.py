# -*- coding utf-8 -*-

from __future__ import annotations
from battle_array import BattleArray


class Player:
    """
    Класс Игрок. 
    Содержит атрибуты:
        player_id - идентификатор игрока
        first_name - имя игрока
        chat_id - идентификатор чата.
        battle_array - поле своих кораблей
        info_array - поле предполагаемого расположения кораблей противника
        enemy - ссылка на противника
    Содержит методы:
        def __init__(self, bot, player_id, first_name, chat_id): Конструктор
        геттеры и сеттеры игрового и информационного полей
    """

    def __init__(self, player_id: int, first_name: str, chat_id: int) -> None:
        self.player_id = player_id
        self.first_name = first_name
        self.chat_id = chat_id
        self.battle_array = None
        self.info_array = None
        self.enemy = None

    async def set_battle_array(self) -> None:
        self.battle_array = BattleArray(self)

    async def get_battle_array(self) -> BattleArray:
        return self.battle_array

    async def set_info_array(self) -> None:
        self.info_array = BattleArray(self)

    async def get_info_array(self) -> BattleArray:
        return self.info_array

    async def set_enemy(self, enemy: Player) -> None:
        self.enemy = enemy

    async def get_enemy(self) -> Player:
        return self.enemy