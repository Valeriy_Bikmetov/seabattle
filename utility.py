# -*- coding utf-8 -*-

import uuid
import constants as const


def check_cell_name(name: str) -> bool:
    """ Проверяем коррктность наименования ячейки игрового поля"""
    if len(name) < 2:
        return  False
    s0 = name[0]
    if s0 == '#' or s0 not in const.title:
        return False
    s0 = name[1:]
    try:
        num = int(s0)
        if num < 1 or num > 10:
            return False
    except ValueError:
        return  False

def check_boat_coordinate(coordinate: str) -> list:
    result = list()
    try:
        s0, s1 = coordinate.split('-')
        s0 = s0.strip()
        s1 = s1.strip()
        if not check_cell_name(s0) or not check_cell_name(s1):
            return result
    except ValueError:
        return result
    begin_r, begin_c = coordinate_from_str(s0)
    end_r, end_c = coordinate_from_str(s1)
    if begin_r == end_r and begin_c != end_c:  # Горизонтальный корабль
        num_descks = end_c - begin_c + 1  # Длина корабля (количество палуб)
        if num_descks > 4 or num_descks < 1:  # Неправильная длина корабля
            return result
        result.append(num_descks)
        temp: list = [(begin_r, begin_c + i) for i in range(num_descks)]
        result.append(temp)
        return result
    elif begin_r != end_r and begin_c == end_c:  # Вертикальный корабль
        num_descks = end_r - begin_r + 1  # Длина корабля (количество палуб)
        if num_descks > 4 or num_descks < 1:  # Неправильная длина корабля
            return result
        result.append(num_descks)
        temp = [(begin_r + i, begin_c) for i in range(num_descks)]
        result.append(temp)
        return result
    else:
        return result  # Неправильный корабль

def coordinate_from_str(cell: str) -> tuple:
    """ Преобразуем строку координат ячейки в реальные строку и колонку"""
    s0 = cell[0]
    s1 = cell[1:]
    column = const.title.index(s0)
    row = int(s1)
    return row, column

def generate_uuid() -> str:
    uid = uuid.uuid4()
    return str(uid)

# Получить текущее время в милисекундах
# current_milli_time = lambda: int(round(time.time() * 1000))
# далее вызов current_milli_time()