# -*- coding utf-8 -*-

from __future__ import annotations
from utility import generate_uuid

class Round:
    """
    Класс раунда игры
    Содержит атрибуты:
        round_id - уникальный идентификатор раунда
        first - игрок
        second - игрок
        геттеры и сеттеры игроков
    """
    def __init__(self, player_first: 'Player', player_second: 'Player'):
        self.first = player_first
        self.second = player_second
        self.first.set_enemy(self.second)
        self.second.set_enemy(self.first)
        self.round_id = generate_uuid()

